// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 4.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const TIMESTAMP = new RegExp(
    /\[\d\d\d\d-(\d\d)-(\d\d) (\d\d):(\d\d)\]/
);
const GUARD = new RegExp(
    /Guard #(\d+) begins shift/
);
const ASLEEP = new RegExp(
    /falls asleep/
);
const AWAKE = new RegExp(
    /wakes up/
);

// Figure out the start day in the year for each month (zero-based).
const MONTH_DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
let month_start = [0];
for (let i = 1; i < MONTH_DAYS.length - 1; i++) {
    month_start.push(month_start[i - 1] + MONTH_DAYS[i - 1]);
}

// XXX Crossing Feb 29 on a leap year will cause a problem.
// Timestamp ignores year.
function timestamp(line) {
    let fields = TIMESTAMP.exec(line);
    if (fields === null)
        throw new Error(`no timestamp: ${line}`);
    let minute = +fields[4];
    let hour = +fields[3];
    let day = +fields[2];
    let month = +fields[1];
    return minute + 60 * (hour + 24 * (day - 1 + month_start[month - 1]));
}

function Entry(line) {
    this.t = timestamp(line);
    this.l = line;
    return this;
}

function timestamp_and_sort(lines) {
    let entries = [];
    for (let i = 0; i < lines.length; i++)
        entries.push(new Entry(lines[i]));
    entries.sort((a, b) => a.t - b.t);
    return entries;
}

// XXX Requires a valid starting line (guard on duty).
// Requires matched pairs of sleep-wake.
function make_schedule(lines) {
    const entries = timestamp_and_sort(lines);

    let asleep = null;
    let g = null;
    let s = null;
    let schedule = {};
    for (let i = 0; i < entries.length; i++) {
        const e = entries[i];
        let ng;
        if (ng = GUARD.exec(e.l)) {
            g = +ng[1];
        } else if (ASLEEP.exec(e.l)) {
            s = e.t;
        } else if (AWAKE.exec(e.l)) {
            if (s === null)
                throw new Error(`wake before sleep: ${e.l}`);
            if (g === null)
                throw new Error(`no guard yet: ${e.l}`);
            if (g in schedule)
                schedule[g].push([s, e.t]);
            else
                schedule[g] = [[s, e.t]];
            s = null;
        } else {
                throw new Error(`mismatched line: ${e.l}`);
        }
    }
    return schedule;
}

function sleeps_by_minute(sleeps) {
    let sleep_minutes = new Array(60).fill(0);
    for (let i = 0; i < sleeps.length; i++) {
        let [sleep_t, awake_t] = sleeps[i];
        sleep_t %= 60;
        awake_t %= 60;
        if (sleep_t > awake_t)
            throw new Error("awake minute before sleep minute");
        for (let j = sleep_t; j < awake_t; j++)
            sleep_minutes[j]++;
    }
    return sleep_minutes;
}

// Assumes no ties.
function part1(lines) {
    const schedule = make_schedule(lines);

    // Find each guard's sleep time.
    let sleep_time = {};
    for (let g in schedule) {
        let t = 0;
        let sleeps = schedule[g];
        for (let i = 0; i < sleeps.length; i++) {
            let [sleep_t, awake_t] = sleeps[i];
            t += awake_t - sleep_t;
        }
        sleep_time[g] = t;
    }

    // Find the longest-sleeping guard.
    let t_max = 0;
    let g_max = null;
    for (let g in sleep_time) {
        let t = sleep_time[g];
        if (t > t_max) {
            t_max = t;
            g_max = g;
        }
    }

    // Find the sleepiest minute for the sleepiest guard.
    let sleep_minutes = sleeps_by_minute(schedule[g_max]);
    let minutes_max = 0;
    let sleep_max = 0;
    for (let i = 0; i < 60; i++) {
        if (sleep_minutes[i] >= minutes_max) {
            minutes_max = sleep_minutes[i];
            sleep_max = i;
        }
    }

    // Produce the required number.
    print(g_max * sleep_max);
}

function part2(lines) {
    const schedule = make_schedule(lines);

    // Find the guard that sleeps the most times on any one
    // minute, the number of times, and the minute.
    let g_max;
    let m_max;
    let t_max = 0;
    for (let g in schedule) {
        let sleep_minutes = sleeps_by_minute(schedule[g]);
        for (let i = 0; i < 60; i++) {
            if (sleep_minutes[i] >= t_max) {
                g_max = g;
                m_max = i;
                t_max = sleep_minutes[i];
            }
        }
    }

    print(g_max * m_max);
}

run_part();
