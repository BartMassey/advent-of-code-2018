# Advent of Code 2018: Day 4
Copyright (c) 2018 Bart Massey

This one was a lot of tedious input processing code, then a
lot of tedious IT code. The given example saved me debugging
a few mistakes: I made a lot of them in this code.

Shuffling the input seems like a gratuitous complication to
me. I chose to ignore most of the weird date constraints,
and produced a timestamp giving the minute within the year
for each line. This made sorting easy. The minute within the
target hour can then be calculated by taking the timestamp
mod 60. (The code could be easily modified for minute within
the day by doing mod 60 * 24 instead.) Calculating the
timestamp properly implied calculating the start day of each
month, which is a simple dynamic programming trick. (Just
assuming months have 31 days would have worked just as well,
I think, but still...)

Javascript things I learned:

* Sorting with a key function.
* `Array` `fill()` to get a zero-filled array.

There are a lot of edge cases this code still won't handle
right:

* Proper timestamps for leap years are not necessary here
  and not supported.

* The input must start with a guard duty line. The sleeps
  and wakes must be properly matched.

* There is no check for ties: it is assumed that there will
  be none.

Because I went a bit overboard on Part 1, Part 2 just
required a simple bit of refactoring and a few lines.

I pre-shuffled the included test (from the problem page) for
better debugging.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
