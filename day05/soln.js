// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 5.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

// Build a regex to recognize all reacting pairs.
let re = [];
for (let i = 0; i < 26; i++) {
    const lower = chrn_lower(i);
    const upper = chrn_upper(i);
    re.push(lower + upper);
    re.push(upper + lower);
}
const PAIRS = new RegExp(re.join('|'), 'g');

// Build regexes matching each individual character type.
re = [];
for (let i = 0; i < 26; i++) {
    const lower = chrn_lower(i);
    re.push(new RegExp(lower, 'ig'));
}
const SINGLETONS = re;

function react(polymer) {
    let n = polymer.length;
    while (true) {
        polymer = polymer.replace(PAIRS, '');
        const m = polymer.length;
        if (m >= n)
            return n;
        n = m;
    }
}

function part1(lines) {
    print(react(lines[0]));
}

function part2(lines) {
    const polymer = lines[0];
    let m = polymer.length;
    for (let i = 0; i < SINGLETONS.length; i++) {
        const re = SINGLETONS[i];
        let mut_polymer = polymer.replace(re, '');
        m = Math.min(m, react(mut_polymer));
    }
    print(m);
}

run_part();
