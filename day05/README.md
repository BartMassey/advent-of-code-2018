# Advent of Code 2018: Day 5
Copyright (c) 2018 Bart Massey

This was a tiny program compared to the way I did the last
one. I made some dumb coding mistakes, but still finished in
a reasonable time. Part 2 required only a trivial refactor
of part 1.

I chose to keep the string representation and use
regexps to manipulate it with `replace()`. This worked well
and gave reasonable runtimes, though it required
constructing regexps on the fly.

Things I learned about Javascript:

* The awkward way of converting between characters and
  character codes.
* Not to ever again try to use `in` to traverse an array.
* The awkward Java-style `Math.min()` method.
* `String.join()`

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
