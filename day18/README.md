# Advent of Code 2018: Day 18
Copyright (c) 2018 Bart Massey

This is a very straightforward cellular automaton. The only
trick is Part 2. You have to realize that the automaton is
likely to repeat states, which I did pretty much right
away. By keeping a history you can skip forward through all
the cycles.

The calculation about how far to skip forward is fiddly: I
did it wrong for hours. I finally grabbed
[this C++ solution](https://www.reddit.com/r/adventofcode/comments/a77xq6/2018_day_18_solutions/ec1109q/)
off Reddit and used it to check my work.  Turned out to be
an off-by-one in the skip, which I swear I'd looked for. I
don't know: I think I've forgotten how to program a
computer.

Things I learned about Javascript: learning to live with
hash tables that can only be keyed by scalars. Hooray
gratuitous string conversions!

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
