// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 18.

"use strict";

load('../lib/aoc.js');

const TRACING = false;

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

class Area {

    constructor(height, width, initializer) {
        this.height = height;
        this.width = width;
        this.history = {};
        this.map = make_2d_array(
            this.height,
            this.width,
            initializer,
        );
    }

    countNeighbors(r, c) {
        let counts = {
            '.': 0,
            '|': 0,
            '#': 0,
        };
        for (let xr = r - 1; xr <= r + 1; xr++) {
            for (let xc = c - 1; xc <= c + 1; xc++) {
                if (xr === r && xc === c)
                    continue;
                if (xr < 0 || xc < 0)
                    continue;
                if (xr >= this.height || xc >= this.width)
                    continue;
                counts[this.map[xr][xc]]++;
            }
        }
        return counts;       
    }

    countSquares() {
        let counts = {
            '.': 0,
            '|': 0,
            '#': 0,
        };
        for (const [s] of this.map)
            counts[s]++;
        return counts;       
    }

    evolveSquare(r, c) {
        const counts = this.countNeighbors(r, c);
        const s = this.map[r][c];
        switch (s) {
        case '.':
            if (counts['|'] >= 3)
                return '|';
            return s;
        case '|':
            if (counts['#'] >= 3)
                return '#';
            return s;
        case '#':
            if (counts['#'] === 0 || counts['|'] === 0)
                return '.';
            return s;
        }
        throw new Error("unexpected square");
    }

    evolve() {
        const map = make_2d_array(
            this.height,
            this.width,
            (r, c) => this.evolveSquare(r, c),
        );
        this.map = map;
    }

    evolution(minutes) {
        let t = 0;
        while (t < minutes) {
            this.history[this.map.toString()] = t;
            this.evolve();
            const key = this.map.toString();
            if (key in this.history) {
                const t0 = this.history[key];
                trace("time", t, "repeated", t0);
                if (t0 === t) {
                    trace("stopping");
                    t = minutes;
                    break;
                }
                const remaining = minutes - t;
                const cycle = t - t0 + 1;
                const skip = cycle * Math.floor(remaining / cycle);
                trace("remaining", remaining, "cycle", cycle, "skip", skip);
                t += skip + 1;
                this.history = {};
                trace("skipping to", t);
                continue;
            }
            trace("---", t);
            trace(this);
            t++;
        }

        const counts = this.countSquares();
        const trees = counts['|'];
        const lumberyards = counts['#'];
        trace("result", t, trees, lumberyards);
        print(trees * lumberyards);
    }

    toString() {
        return this.map.toString();
    }
}

function read_area(lines) {
    return new Area(
        lines.length,
        lines[0].length,
        (r, c) => lines[r][c],
    );
}

function part1(lines) {
    let area = read_area(lines);
    trace(area);

    area.evolution(10);
}

function part2(lines) {
    let area = read_area(lines);
    area.evolution(1000000000);
}

run_part();
