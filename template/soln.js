// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day <day>.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

function part1(lines) {
    throw new Error("part 1 not yet implemented");
}

function part2(lines) {
    throw new Error("part 2 not yet implemented");
}

run_part();
