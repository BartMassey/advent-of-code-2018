# Advent of Code 2018: Day <day>
Copyright (c) 2018 Bart Massey

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
