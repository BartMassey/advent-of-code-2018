// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 1.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

function part1(lines) {
    let level = 0;
    for (const l of lines)
        level += +l;
    print(level);
}

function part2(lines) {
    let reached = new Map();
    let level = 0;
    let i = 0;
    while (true) {
        level += +lines[i];
        if (level in reached) {
            print(level);
            return;
        }
        reached[level] = true;
        i++;
        if (i >= lines.length)
            i = 0;
    }
}

run_part();
