# Advent of Code 2018: Day 1
Copyright (c) 2018 Bart Massey

This was a couple of nice warmup exercises that helped me to
understand how to get command-line Javascript up and working
on my box.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
