# Advent of Code 2018: Day 19
Copyright (c) 2018 Bart Massey

OK, the reverse engineering problem. Knew it was
coming. Heard there's another one coming, which makes me
sad.

I solved Part 1 with brute force. Part 2 was not
susceptible, hence the reverse engineering. I
hand-translated the assembly to Javascript, fixing my many
mistakes as I went, then fixed the inner loop to do a divmod
rather than counting up in trial division.

Run with

    smjs input.js 1
    smjs input.js 2

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
