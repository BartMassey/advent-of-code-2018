// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 19.

"use strict";

const TRACING = true;

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const NREGS = 6;

const REG = Symbol("reg");
const IMM = Symbol("imm");
const IGN = Symbol("ign");

class Arg {
    constructor(letter, posn) {
        this.letter = letter;
        this.posn = posn;
        switch (letter) {
        case '-': this.type = IGN; break;
        case 'r': this.type = REG; break;
        case 'i': this.type = IMM; break;
        default: throw new Error("unknown arg type: " + letter);
        }
    }

    get() {
        return this.type;
    }

    reg(regs) {
        return regs[this.posn];
    }

    toString() {
        return this.letter;
    }
}

let ip_reg;
let opcode_table = {};

class Op {
    constructor(name, args, op) {
        this.name = name;
        this.args = args.split('').map((l, i) => new Arg(l, i));
        this.op = op;
        opcode_table[this.name] = this;
    }

    exec(opds, regs) {
        // trace("exec", this.name, ...opds);
        // trace(" before", regs);
        const opargs = [];
        this.args.forEach((a, i) => {
            switch (a.type) {
            case IMM: opargs.push(opds[i]); break;
            case REG: {
                if (opds[i] < 0 || opds[i] >= NREGS)
                    throw new Error("bad register spec");
                opargs.push(regs[opds[i]]);
                break; }
            case IGN: break;
            default: throw new Error("bad argument type");
            }
        });
        regs[opds[opds.length - 1]] = this.op(...opargs);
        trace(" after", regs);
    }

    toString() {
        return this.name + " " + this.args.join(' ');
    }
}

const ops = [
    new Op("addr", "rr", (x, y) => x + y),
    new Op("addi", "ri", (x, y) => x + y),
    new Op("mulr", "rr", (x, y) => x * y),
    new Op("muli", "ri", (x, y) => x * y),
    new Op("banr", "rr", (x, y) => x & y),
    new Op("bani", "ri", (x, y) => x & y),
    new Op("borr", "rr", (x, y) => x | y),
    new Op("bori", "ri", (x, y) => x | y),
    new Op("setr", "r-", (x) => x),
    new Op("seti", "i-", (x) => x),
    new Op("gtir", "ir", (x, y) => +(x > y)),
    new Op("gtri", "ri", (x, y) => +(x > y)),
    new Op("gtrr", "rr", (x, y) => +(x > y)),
    new Op("eqir", "ir", (x, y) => +(x === y)),
    new Op("eqri", "ri", (x, y) => +(x === y)),
    new Op("eqrr", "rr", (x, y) => +(x === y)),
];

class Regs {
    constructor(...regs) {
        this.regs = regs.slice();
        return this;
    }

    get() {
        return this.regs;
    }

    equals(r) {
        for (let i = 0; i < r.length; i++)
            if (this.regs[i] !== r[i])
                return false;
        return true;
    }
    
    toString() {
        return "[" + this + "]";
    }
}

class Program {
    constructor(ip_reg, insns) {
        this.ip_reg = ip_reg;
        this.insns = insns;
    }

    run(regs) {
        while (true) {
            const ip = regs[this.ip_reg];
            if (ip < 0 || ip >= this.insns.length) {
                trace("halting", regs);
                return;
            }
            const insn = this.insns[ip];
            const op = opcode_table[insn.name];
            trace("-", ip + ":", insn.name, ...insn.opds);
            op.exec(insn.opds, regs);
            regs[this.ip_reg]++;
        }
    }

    toString() {
        return "#ip " + this.ip_reg + "\n" + this.insns.join('\n');
    }
}

class Insn {
    constructor(line) {
        const fields = line.split(' ');
        this.name = fields[0];
        this.opds = fields.slice(1).map(s => parseInt(s));
        return this;
    }

    toString() {
        return this.name + ' ' + this.opds.join(' ');
    }
}

function read_program(lines) {
    const ip_reg = parseInt(lines[0].match(/^#ip ([0-6])$/)[1]);
    const insns = lines.slice(1).map(s => new Insn(s));
    return new Program(ip_reg, insns);
}

function part1(lines) {
    const program = read_program(lines);
    let regs = new Array(NREGS).fill(0);
    program.run(regs);
    print(regs[0]);
}


function part2(lines) {
    const program = read_program(lines);
    let regs = new Array(NREGS).fill(0);
    regs[0] = 1;
    program.run(regs);
    print(regs[0]);
}

run_part();
