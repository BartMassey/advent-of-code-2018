// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 19.

"use strict";

const TRACING = true;

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

function calc(r0) {
    if (r0 > 1)
        throw new Error("range");
    let r1;
    let r2;
    let r3;
    let r4;
    r3 = 2;
    r3 *= r3;
    r3 *= 19;
    r3 *= 11;
    r4 = 6;
    r4 *= 22;
    r4 += 5;
    r3 += r4;
    if (r0 === 1) {
        r4 = 27;
        r4 *= 28;
        r4 += 29;
        r4 *= 30;
        r4 *= 14;
        r4 *= 32;
        r3 += r4;
        r0 = 0;
    }

    r2 = 1;
    do {
        if (r3 % r2 === 0)
            r0 += r3 / r2;
        r2 += 2;
    } while (r2 <= r3);

    return r0;
}

function part1() {
    print(calc(0));
}

function part2() {
    print(calc(1));
}

run_part(false);
