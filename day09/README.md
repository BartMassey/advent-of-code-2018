# Advent of Code 2018: Day 9
Copyright (c) 2018 Bart Massey

Sigh. Circularly-linked lists. I dorked insertion the first
time, which really slowed me down.

At least there's no huge efficiency concern here, unlike the
similar problem from a previous year: an *O(n)* solution
where *n* is the number of marbles is pretty easy to
achieve. I suspect there's a way to get a huge constant
factor speedup by careful meditation. I'm just not up for
it, though.

Oh and hey! Here's a new way to sneak a free parameter into
Part 2 without including it in the input! I just gave up and
hardcoded `100` into the solution.

The timing information when running Part 2 under `d8` is
interesting.

    $ time d8 -- soln.js 2 <input.txt 
    3180373421

    real    0m1.826s
    user    0m4.641s
    sys     0m0.206s

This strongly suggests that `d8` has decided to multithread
somehow and run this thing on multiple cores, which is
weird but kind of cool.

We can compare with the times for SpiderMonkey, after
hacking the last call in the code to use the alternative
variable name for the command-line arguments (`arguments` in
`d8`, `scriptArgs` in `smjs`).

    $ time smjs soln.js 2 <input.txt 
    3180373421

    real    0m0.853s
    user    0m0.755s
    sys     0m0.140s

Looking at the performance difference, I decided to just
switch to `smjs` for all days. It's also a bit cleaner and
installs out-of-the-box on Debian: see the top-level
[README](../README.md) for details.

Things I learned about Javascript:

* How to write an object using external methods. (I'd
  already figured this out, but it's nice to know it works.)

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
