// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 9.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

let nplayers;
let nmarbles;

function read_params(lines) {
    let m = lines[0].match(/(\d+)[^\d]+(\d+)/);
    nplayers = +m[1];
    nmarbles = +m[2];
}

function neighbor_m(relpos) {
    let target = this;
    if (relpos >= 0) {
        while(relpos > 0) {
            target = target.next;
            relpos--;
        }
    } else {
        while(relpos < 0) {
            target = target.prev;
            relpos++;
        }
    }
    return target;
}


function insert_m(value) {
    let insertion = new NCLL(value);
    const prev = this.prev;
    prev.next = insertion;
    this.prev = insertion;
    insertion.next = this;
    insertion.prev = prev;
    return insertion;
}

function delete_m() {
    let next = this.next;
    let prev = this.prev;
    prev.next = next;
    next.prev = prev;
    return [this.value, next];
}

function NCLL(value) {
    this.value = value;
    this.next = this;
    this.prev = this;

    this.neighbor_m = neighbor_m;
    this.insert_m = insert_m;
    this.delete_m = delete_m;
    return this;
}

function run_game() {
    let scores = new Array(nplayers).fill(0);
    let player = 0;
    let current = new NCLL(0);
    for (let i = 1; i <= nmarbles; i++) {
        if (i % 23 == 0) {
            scores[player] += i;
            current = current.neighbor_m(-7);
            const [v, c] = current.delete_m();
            scores[player] += v;
            current = c;
        } else {
            current = current.neighbor_m(2);
            //print(current.value);
            current = current.insert_m(i);
        }
        player += 1;
        if (player === nplayers)
            player = 0;
    }
    print(Math.max(...scores));
}

function part1(lines) {
    read_params(lines);
    run_game();
}

function part2(lines) {
    read_params(lines);
    nmarbles *= 100;
    run_game();
}

run_part();
