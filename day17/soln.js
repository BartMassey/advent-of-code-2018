// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 17.

"use strict";

const TRACING = false;

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

class Map {
    constructor(r_min, r_max, c_min, c_max, clay) {
        this.r_min = r_min;
        this.r_max = r_max;
        this.c_min = c_min;
        this.c_max = c_max;
        this.height = r_max - r_min + 1;
        this.width = c_max - c_min + 1;

        this.map = make_2d_array(this.height, this.width, () => '.');
        for (const [c, r] of clay)
            this.map[r - this.r_min][c - this.c_min] = '#';

        return this;
    }

    labelAt(r, c) {
        return this.map[r][c];
    }

    dropWater(r, c) {
        let moving = [];
        this.map[r][c] = '|';
        moving.push([r, c]);
        while (moving.length > 0) {
            const [r, c] = moving.pop();
            //trace("update", this.map[r][c], r, c);

            const push_neighbors = (xr, xc) => {
                const deltas = [[0, 0], [-1, 0], [1, 0], [0, -1], [0, 1]];
                for (const [dr, dc] of deltas) {
                    const xxr = xr + dr;
                    if (xxr >= this.height)
                        continue;
                    const xxc = xc + dc;
                    const label = this.map[xxr][xxc];
                    if (label === '#')
                        continue;
                    moving.push([xxr, xxc]);
                }
            };

            // Handle moving water.
            if (this.map[r][c] === '|' && r < this.height - 1) {
                const support = this.map[r + 1][c];

                // Drop moving water on empty space.
                if (support === '.') {
                    this.map[r + 1][c] = '|';
                    push_neighbors(r + 1, c);
                    continue;
                }

                // For supported moving water, do a couple of things.
                if (support === '#' || support === '~') {

                    // 1. Drop moving water into empty space
                    // left and right.
                    for (const xc of [c - 1, c + 1]) {
                        if (this.map[r][xc] === '.') {
                            this.map[r][xc] = '|';
                            push_neighbors(r, xc);
                        }
                    }

                    // 2. Transform this moving water into
                    // still if it is fully supported left
                    // and right.
                    const has_support = (dc) => {
                        // XXX Will stop at edge if needed
                        // because it is unsupported.
                        for (let xc = c + dc; true; xc += dc) {
                            if (this.map[r][xc] === '#')
                                return true;
                            const support = this.map[r + 1][xc];
                            if (support !== '#' && support !== '~')
                                return false;
                        }
                    };

                    if (has_support(-1) && has_support(1)) {
                        this.map[r][c] = '~';
                        push_neighbors(r, c);
                        continue;
                    }
                }
            }

            // Spread still water.
            if (this.map[r][c] === '~') {
                let neighbors = [[r, c-1], [r, c+1]];
                if (r < this.height - 1)
                    neighbors.push([r + 1, c]);
                for (const [xr, xc] of neighbors) {
                    if (this.map[xr][xc] === '.') {
                        this.map[xr][xc] = '~';
                        push_neighbors(xr, xc);
                    }
                }
            }
        }
    }

    countWater(moving = true) {
        let t = 0;
        for (const [s] of this.map) {
            switch (s) {
            case '~': t++; break;
            case '|': if (moving) t++; break;
            }
        }
        return t;
    }

    toString() {
        let result = "r=" + this.r_min + ".." + this.r_max +
            " c=" + this.c_min + ".." + this.c_max + "\n";
        for (let r = 0; r < this.height; r++) {
            for (let c = 0; c < this.width; c++)
                result += this.labelAt(r, c);
            result += '\n';
        }
        return result;
    }
}

function read_map(lines) {
    let clay = [];
    for (const line of lines) {
        let m = line.match(/^x=([0-9]+), y=([0-9]+)\.\.([0-9]+)$/);
        if (m !== null) {
            for (let y = +m[2]; y <= +m[3]; y++)
                clay.push([+m[1], y]);
            continue;
        }
        m = line.match(/^y=([0-9]+), x=([0-9]+)\.\.([0-9]+)$/);
        if (m !== null) {
            for (let x = +m[2]; x <= +m[3]; x++)
                clay.push([x, +m[1]]);
            continue;
        }
        throw new Error("bad line: " + line);
    }

    const xs = clay.map(([x, _]) => x);
    const ys = clay.map(([_, y]) => y);
    const c_min = Math.min(...xs);
    const c_max = Math.max(...xs);
    const r_min = Math.min(...ys);
    const r_max = Math.max(...ys);
    return new Map(r_min, r_max, c_min - 1, c_max + 1, clay);
}

function part1(lines) {
    const map = read_map(lines);
    map.dropWater(0, 500 - map.c_min);
    // trace(map);
    print(map.countWater(true));
}

function part2(lines) {
    const map = read_map(lines);
    map.dropWater(0, 500 - map.c_min);
    print(map.countWater(false));
}

run_part();
