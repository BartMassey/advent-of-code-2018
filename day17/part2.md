\-\-- Part Two \-\-- {#part2}

After [ a very long
time]{title="A very long time ago, I built something similar to this in QBasic. SCREEN 13 forever!"},
the water spring will run dry. How much water will be retained?

In the example above, water that won\'t eventually drain out is shown as
`~`, a total of `29` tiles.

*How many water tiles are left* after the water spring stops producing
water and all remaining water not at rest has drained?

Your puzzle answer was `24660`.
