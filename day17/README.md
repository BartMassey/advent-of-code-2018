# Advent of Code 2018: Day 17
Copyright (c) 2018 Bart Massey

Sigh. I spent way too long trying to do this with clever
spans for an efficient map and then with recursion. When I
finally gave up and looked at Reddit, I found
[this comment](https://www.reddit.com/r/adventofcode/comments/a6wpup/2018_day_17_solutions/ebys4h0/)
which suggested an update stack.  Doh. Of course.

Things I learned about Javascript: not much.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
