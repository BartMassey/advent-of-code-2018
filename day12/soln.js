// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 12.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

// XXX See https://stackoverflow.com/a/43811878 for a way to
// overload [] sort of. Ugh. I'll just go without.

function Tape(itape = []) {
    this.left = new Array();
    this.right = new Array();
    this.bounds = null;
    this.offset = 0;

    this.get = function(i) {
        i -= this.offset;
        if (i >= 0) {
            if (i < this.right.length)
                return this.right[i];
            else
                return false;
        } else {
            i = -i + 1;
            if (i < this.left.length)
                return this.left[i];
            else
                return false;
        }
    }

    this.put = function(i, b) {
        i -= this.offset;
        if (b === true) {
            if (this.bounds === null)
                this.bounds = [i, i];
            if (i < this.bounds[0])
                this.bounds[0] = i;
            if (i >= this.bounds[1])
                this.bounds[1] = i + 1;
        }
        if (i >= 0) {
            while (this.right.length <= i)
                this.right.push(false);
            this.right[i] = b;
        } else {
            i = -i + 1;
            while (this.left.length <= i)
                this.left.push(false);
            this.left[i] = b;
        }
    }

    this.toArray = function() {
        let result = [];
        for (let i = this.bounds[0]; i < this.bounds[1]; i++)
            result.push(this.get(i));
        return result;
    }

    this.shift = function(s) {
        this.offset += s;
        if (this.bounds !== null) {
            this.bounds[0] += s;
            this.bounds[1] += s;
        }
    }

    for (let i = 0; i < itape.length; i++)
        this.put(i, itape[i]);

    return this;
}


function read_problem(lines) {
    if (lines.length !== 34)
        throw new Error(`unexpected line count ${lines.length}`);

    function to_bool(c) {
        if (c === '.')
            return false;
        if (c === '#')
            return true;
        throw new Error(`to_bool: unexpected char ${c}`);
    }

    let itape = lines[0].match(/^initial state: ([\.#]+)$/);
    if (itape === null)
        throw new Error("cannot match on initial state");
    itape = [...itape[1]].map(to_bool);

    const subst = new Object();
    for (let i = 2; i < 34; i++) {
        const m = lines[i].match(/([\.#]+) => ([\.#])/);
        if (m === null)
            throw new Error(`cannot match on rule "${lines[i]}"`);
        const [, m_lhs, m_rhs] = m;
        const lhs = [...m_lhs].map(to_bool);
        const rhs = to_bool(m_rhs);
        subst[lhs] = rhs;
    }
    return [itape, subst];
}

function run_until(generation, itape, subst) {
    let memo = new Object();
    memo[itape] = 0;
    let tape = new Tape(itape);
    for (let i = 1; i <= generation; i++) {
        let [start, end] = tape.bounds;
        let next_tape = new Tape();
        for (let j = start - 4; j < end + 4; j++) {
            let pat = [];
            for (let k = j - 2; k <= j + 2; k++)
                pat.push(tape.get(k));
            next_tape.put(j, subst[pat]);
        }
        tape = next_tape;
        const ta = tape.toArray().toString();
        if (ta in memo) {
            const [old_t, old_bounds] = memo[ta];
            const offset = tape.bounds[0] - old_bounds[0];
            const cycle = i - old_t;
            const skipped = Math.floor((generation - i) / cycle);
            i += skipped;
            tape.shift(offset * skipped);
            continue;
        }
        memo[ta] = [i, [tape.bounds[0], tape.bounds[1]]];
        if (i % 1000 === 0)
            print(`gen ${i}[${tape.bounds}]`);
    }
    const [start, end] = tape.bounds;
    let t = 0;
    for (let i = start; i < end; i++)
        t += i * tape.get(i);
    print(t);
}

function part1(lines) {
    run_until(20, ...read_problem(lines));
}

function part2(lines) {
    run_until(50e9, ...read_problem(lines))
}

run_part();
