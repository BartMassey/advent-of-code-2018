# Advent of Code 2018: Day 12
Copyright (c) 2018 Bart Massey

Todays problem is brought to you by Cellular Automata.
*Cellular Automata! They may be useless in the real world,
but they make fantastic puzzle problems.*

OK, I'm out for tonight.

It took me a few hours to finish Part 1: a few hours of
typing late at night on my cramped laptop screen in a hotel
far from home. I built a whole new datastructure for the CA
tape, and did some nice engineering. My code ran and gave
the correct answer on the first try at the input instance.
Then I looked at Part 2.

I wrote last night:

> The "50 billion" for Part2 pretty much precludes using my
> eerily slow solution to brute force anything, even with the
> obvious speedups. There's obviously an algorithmic trick,
> perhaps involving cycle times of interior nodes. In fact, I
> think I've implemented and taught that trick long ago in one
> of my classes? Anyhow, I'm not too interested in re-figuring
> it out. I'll wait for the gurus on the Internets to tell me
> the trick tomorrow and finish then.
> 
> 1 out of 2 *s.

After writing this I thought about it some more and realized
I should look for complete cycles in the input. I
implemented the obvious memoization, and ran it: never
repeated.  I posted for help from the Internet. Before I got
any (I never did, really) I looked at the Reddit thread
which said I should look for complete cycles. Then I went
back and found the bug in my code.

After implementing the rest of the answer and fixing a
couple more pernicious bugs I finally finished Part 2 a day
late and a dollar short.

Things I learned about Javascript:

* Thing you read on the Internet might be wrong. `~~(x/y)`
  has been reported as integer division of `x` and `y`. For
  large values, it doesn't work. Only choice I've found is
  use `Math.floor(x/y)` and hope for no floating-point
  error.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
