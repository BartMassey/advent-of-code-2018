// re   ::= seq alt  (first = "|(NSEW"; follow = "$")
// alt  ::= '|' seq alt |  (first = "|"; follow = "$)")
// seq  ::= dirn seq | '(' re ')' seq |  (first="(NSEW"; follow = "$|")
// dirn ::= 'N' | 'S' | 'E' | 'W'

function peek(input) {
    if (input.length === 0)
        throw new Error("input ended early");
    return input[input.length - 1];
}

class NFA {
    constructor(re) {
        let input = re.split('');
        input.push('$');
        input.reverse();
        this.nfa = this.parse_re(input);
        if (input !== '$')
            throw new Error("re terminated early");
    }

    parse_re(input) {
        const s = this.parse_seq(input);
        const ss = this.parse_alt(input);
        return [s, ...ss];
    }

    parse_alt(input) {
        switch(peek(input)) {
        case '|':
            input.pop();
            const s = this.parse_seq(input);
            const ss = this.parse_alt(input);
            return [s, ...ss];
        default:
            return [];
        }
    }

    parse_seq(input) {
        switch (peek(input)) {
        case 'N':
        case 'S':
        case 'E':
        case 'W':
            const next = input.pop();
            const rest = this.parse_seq(input);
            return {[next]: rest};
        case '(':
            input.pop();
            const m = this.parse_re(input);
            switch (peek(input)) {
            case ')':
                input.pop();
                return {'(': m};
            default:
                throw new Error("seq: unclosed parens");
            }
        case '$':
        case '|':
        case ')':
            return null;
        default:
            throw new Error("seq bad end " + peek(input));
        }
    }

    formatRe(re) {
        if (re === null)
            return "";
        if (typeof re !== "object")
            throw new Error("formatRe: bad re " + re);
        for (const d of "NSEW")
            if (d in re)
                return d + this.formatRe(re[d]);
        if ('(' in re)
            return '(' + this.formatRe(re['(']) + ')';
        if (!(0 in re))
            return "";
        let result = this.formatRe(re[0]);
        for (let i = 1; i < re.length; i++)
            result += '|' + this.formatRe(re[i]);
        return result;
    }

    toString() {
        return this.formatRe(this.nfa);
    }
}

let nfa = new NFA(scriptArgs[0]);
print(nfa);
