// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 20.

"use strict";

const TRACING = false;

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const H = Symbol("H");
const V = Symbol("V");

const CONCAT_RE = /^([NSEW]+)(.*)$/;

function* traverse_insns(insns, start, cur, fold_fn) {
    if (insns.length === 0) {
        yield cur;
        return;
    }
    switch (insns[0]) {
    case '(': {
        let count = 1;
        let pr;
        for (pr = 1; pr < insns.length; pr++) {
            switch (insns[pr]) {
            case '(': count++; break;
            case ')': count--; break;
            }
            if (count === 0)
                break;
        }
        if (pr >= insns.length)
            throw new Error("no matching close paren");
        const sub = insns.slice(1, pr);
        const rest = insns.slice(pr+1);
        trace("parens", sub, '/', rest);
        for (const s of traverse_insns(sub, cur, cur, fold_fn))
            for (const r of traverse_insns(rest, start, s, fold_fn))
                yield r; 
        return;
    }
    case '|': {
        const alt = insns.slice(1);
        trace("alt", alt);
        yield cur;
        for (const s of traverse_insns(alt, start, start, fold_fn))
            yield s;
        return;
    }
    case 'N':
    case 'S':
    case 'E':
    case 'W': {
        const match = insns.match(CONCAT_RE);
        if (match === null)
            throw new Error("bad match");
        const first = match[1];
        const rest = match[2];
        const next = [...first].reduce(fold_fn, cur);
        for (const s of traverse_insns(rest, start, next, fold_fn))
            yield s;
        return;
    }
    default:
        throw new Error("unknown symbol in RE", insns[0]);
    }
}

const dirns = {
    'N': [-1,  0, H],
    'S': [ 1,  0, H],
    'E': [ 0,  1, V],
    'W': [ 0, -1, V],
};

class RoomMap {
    constructor() {
        this.doors = {
            [H]: new Set(),
            [V]: new Set(),
        };
        this._dims = null;
    }

    build([r, c], next) {
        trace("build", r, c, next);
        const [dr, dc, d] = dirns[next];
        const ddr = dr === -1 ? 0 : dr;
        const ddc = dc === -1 ? 0 : dc;
        trace("door", d.toString(), r + ddr, c + ddc);
        const x = [r + ddr, c + ddc].toString();
        this.doors[d].add(x);
        this._dims = null;
        return [r + dr, c + dc];
    }

    get dims() {
        if (this._dims === null) {
            trace("finddims", this.doors[H], this.doors[V]);
            const coords = [...this.doors[H]];
            coords.push(...this.doors[V]);
            const rs = coords.map(c => +c.split(',')[0]);
            const cs = coords.map(c => +c.split(',')[1]);
            const ds = (xs) => [Math.min(...xs), Math.max(...xs) + 1];
            this._dims = [ds(rs), ds(cs)];
        }
        trace("dims", this._dims);
        return this._dims;
    }

    toString() {
        let result = "";
        let [[r0, rn], [c0, cn]] = this.dims;
        for (let r = r0; r < rn; r++) {
            for (let c = c0; c < cn; c++) {
                result += '#';
                if (this.doors[H].has([r, c].toString()))
                    result += '-';
                else
                    result += '#';
            }
            result += "#\n";
            for (let c = c0; c < cn; c++) {
                if (this.doors[V].has([r, c].toString()))
                    result += '|';
                else
                    result += '#';
                if (r === 0 && c === 0)
                    result += 'X';
                else
                    result += '.';
            }
            result += "#\n";
        }
        for (let c = c0; c < cn; c++)
            result += "##";
        result += "#";
        return result;
    }
}

function read_map(lines) {
    let map = new RoomMap();
    const build = (s, v) => map.build(s, v);
    for (const s of traverse_insns(lines[0].slice(1, -1), [0, 0], [0, 0], build))
         trace("traverse", ...s);
    return map;
}

function part1(lines) {
    let map = read_map(lines);
    print(map);
}

function part2(lines) {
    throw new Error("part 2 not yet implemented");
}

run_part();
