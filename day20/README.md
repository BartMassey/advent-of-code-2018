# Advent of Code 2018: Day 20
Copyright (c) 2018 Bart Massey

OK, I give up. I'm done with Advent of Code.

Today's problem has an interesting feature: the map for the
actual input cannot be feasibly drawn, as far as I can tell.
This is because multiple traversals of the same square are
allowed by the problem description, so pruning doesn't work.

The example input has many, many alternatives to explore, so
a backtracking search of the RE won't finish in a reasonable
amount of time. However, without doing this it's not
obviously feasible to decide whether the whole map has been
seen.

Consider the map described by "^WNENW|ENWNE$". This map
looks like this:

    #######
    #.|.|.#
    ###-###
    #.|@|.#
    #-###-#
    #.|X|.#
    #######

Note that without fully traversing both REs, you will miss a
room. However, both REs pass through the square marked `@`,
so you can't prune there.

After further consideration, there seems to be a likely
dynamic programming approach to this problem. Judging by the
solutions I've seen on Reddit, this isn't how one solves it,
but it seems like maybe what you'd have to do to solve it
properly. I *suspect* that the RE has a (hidden) constraint
that it never traverses a room twice like the example above
does.

I put maybe 10 hours into getting my fiddly and awful
Javascript map code right, with the goal of *then* doing the
Dijkstra search. Yes, I am a slow programmer.  But I just
can't make myself go on and finish this mess, and I just
can't make myself give up and solve the simpler implied
problem. Even if I finish Day 20 tonight I'll still be 3
days behind, with only three days until Christmas.

Enough of this grind.

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
