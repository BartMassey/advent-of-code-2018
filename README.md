# Advent Of Code 2018: Tutorial Solutions in Javascript
Copyright (c) 2018 Bart Massey

Herein lie command-line Javascript solutions to Days 1-19 of
the 2018 [Advent of Code](http://adventofcode.com). Advent
of Code is a fantastic exercise, and I thank the author and
others involved profusely for their excellent work. Thanks
also to `relsqui` for pointing me at this back in 2015.

The solutions are in directories named `day01` through
`day19`. For each solution, I have included cleaned-up
Javascript code. There is a `README.md` in every problem
directory containing algorithm descriptions, comments and
usage instructions. I have also included the problem
descriptions (`part1.md` and `part2.md`) and my specific
`input.txt` for posterity. There's also a partial solution
in `day20`, but that marks the point where I gave up.

The solutions load library code from the `lib/` directory.
`lib/aoc.js` contains basic machinery for reading inputs and
running parts.

This code was run using a command-line Javascript
interpreter on a fast-ish UNIX box with a bunch of memory
(although most everything should also work on other
operating systems). At Day 9 I left Google's
[V8](http://v8.dev) Javascript engine's
[d8](http://v8.dev/docs/d8) shell in favor of Mozilla's
[SpiderMonkey](http://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey)
[js](http://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey/Introduction_to_the_JavaScript_shell)
shell (installed as `smjs`: see below). `smjs` makes
argument processing easier to work with by not conflating
`arguments` to the program with `arguments` to a function
(because `scriptArgs`). `smjs` also seems to be a bit faster
on most of the longer-running problems, and it can be
installed on my Debian box using normal packaging:

    $ apt-get install libmozjs60-dev
    $ update-alternatives --install /usr/bin/smjs smjs /usr/bin/js60 1200

The installation of `d8` on my box was it's own nightmare: I
hope never to do that again. You can still run the solutions
with `d8` if you like: don't forget to invoke with `--` in
this case, for example

    d8 soln.js -- 1 <input.txt

There are no special tests written for this code other than
the ones provided as part of the problem. I regard passing
both parts of a day's problem as strong validation, although
it turned out that I was wrong about that at least on
Day 6. More tests should get written.

The goals of these solutions are to:

* Provide correct solutions with reasonable runtimes.

* Illustrate reasonable solution strategies.

* Illustrate the use of Javascript in problem-solving.

I learned a ton of Javascript and a little bit of software
engineering I should already have known writing these.

There's also some engineering infrastructure in the form of
the `template` directory and the `mkday.sh` and
`process-aoc.sh` shell scripts.  This sped up each day's
setup considerably. At the beginning of day 1 I would "`sh
mkday.sh 1`". (On subsequent days, the day number will be
tracked automatically and can be omitted.) At the end of the
day I would select and copy the page source of the day 1 AoC
page and then

    xclip -selection CLIPBOARD -out | sh ../process-aoc.sh

to get markdown into the problem files for posterity.

You can get times for all parts of all days with "sh
times.sh". This also verifies that everything runs.  Use "sh
times.sh d8" to get `d8` timings instead of `smjs`.

These solutions deserve a much more thorough top-level
description than I have the energy to write at this point.
I will revise this file in the indefinite future.

---

This work is licensed under the "MIT License".  Please see
the file `LICENSE` in the source distribution of this
software for license terms.
