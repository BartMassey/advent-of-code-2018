// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 7.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const LOG = false;

const LINE_RE =
    /Step ([A-Z]) must be finished before step ([A-Z]) can begin./;

function read_steps(lines) {
    let steps = {};
    for (const l of lines) {
        const m = l.match(LINE_RE);
        const pre = m[1];
        const post = m[2];
        if (m === null)
            throw new Error(`bad line ${line}`);
        if (pre in steps)
            steps[pre] += [post];
        else
            steps[pre] = [post];
    }
    return steps;
}

function find_alphabet(steps) {
    let alphabet = new Set();
    for (const pre in steps) {
        alphabet.add(pre);
        for (const post of steps[pre])
            alphabet.add(post);
    }
    return alphabet;
}

function find_next_steps(steps, completed, candidates) {
    let survived = new Set([...completed]);
    let survivors = new Set([...candidates]);
    for (const pre in steps) {
        if (survived.has(pre))
            continue;
        for (const post of steps[pre])
            survivors.delete(post);
    }
    let next_steps = [...survivors];
    next_steps.sort();
    return next_steps;
}

// Strategy: Source-to-sink topological sort.
function part1(lines) {
    const steps = read_steps(lines);
    let candidates = find_alphabet(steps);

    let insns = [];
    while (true) {
        const nexts = find_next_steps(steps, insns, candidates);
        if (nexts.length === 0)
            break;
        insns += [nexts[0]];
        candidates.delete(nexts[0]);
    }
    print(insns);
}

function duration(step) {
    return 1 + ordn(step);
}

function Task(time, insn) {
    this.time = time;
    this.insn = insn;
    return this;
}

// Strategy: Use the source-to-sink code of Part 1, but with
// a separate set to manage the in-flight tasks.
function part2(lines) {
    const WORKERS = +get_numeric_argument(0, "workers");
    const BASE_TIME = +get_numeric_argument(1, "base-time");

    const steps = read_steps(lines);
    let candidates = find_alphabet(steps);

    let in_flight = new Set();
    let insns = [];
    let t = 0;
    while (true) {
        let completed = [...in_flight]
            .filter(task => task.time <= t);
        if (LOG) {
            print("t:", t);
            print("in-flight:", ...in_flight);
            print("completed:", ...completed);
            print();
        }
        completed.sort((t1, t2) => ord(t1[1]) - ord(t2[1]));
        for (const task of completed) {
            insns += [task.insn];
            in_flight.delete(task);
        }
        const nexts = find_next_steps(steps, insns, candidates);
        for (const insn of nexts) {
            if (in_flight.size >= WORKERS)
                break;
            candidates.delete(insn);
            in_flight.add(new Task(t + BASE_TIME + duration(insn), insn));
        }
        if (in_flight.size === 0)
            break;
        t = Math.min(...[...in_flight].map(task => task.time));
    }
    print(t);
}

run_part();
