# Advent of Code 2018: Day 7
Copyright (c) 2018 Bart Massey

Today's entry wants a source-to-sink topological sort for
Part 1. For Part 2, it wants to augment that sort with
resource management.

Part 1 itself isn't that fiddly, but it is hard to code
something as fancy as a tsort while also learning the
language.

Part 2 is just fiddly. I needed to refactor my Part 1 code a
little to use the tsort step from it. I missed the fact that
the example uses a different number of workers and a
different base task time than the input. I missed that I
should count myself as a worker. I missed that I was
supposed to report the total time rather than the schedule.

All in all, this took me more hours than I really had and
was more than a bit frustrating.

Things I learned about Javascript:

* How to work with the awkward but useful `Set` class.

* More `Array` functional methods.

* How to make a `String` lowercase.

* Javascript's `Object` `delete` operator (which I ended up
  not needing).

* The versatility of the `...` operator.

* To fear the passing of arguments in the `arguments` array.

* Javascript will *never throw an error message,* just
  silently do something stupid.

Debugging is hard in Javascript: I haven't found the
debugger yet, and the native print functions don't print
things in a reasonable way. I would give the trimmings of my
left big toenail to get a native debugging print function
that actually prints things like arrays and objects in a way
that lets me actually read the output.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 5 60 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
