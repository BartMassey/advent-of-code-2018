# Advent of Code 2018: Day 6
Copyright (c) 2018 Bart Massey

The tricks here are to figure out an algorithm that will
produce the correct solution, and to bound the running time
enough that Javascript can compute an answer in a reasonable
time. We're not dealing with Rust here. See the source for
comments sketching the operation of the algorithms.

The running time of the algorithm for Part 1 is *O(kmn)*
where *k* is the number of coordinates, *m* is the distance
between the minimum and maximum *x* coordinate, and *n* is
the distance between the minimum and maximum *y* coordinate.
For Part 2 it is *O(mnb^2/k)* where *b* is the distance
bound. These are not efficient algorithms, but fortunately
they yield reasonable runtimes for the given input. The
curiousity that as *k* gets larger for a given *b* the
program actually gets faster is sort of the salvation of
this code. Part 2 took about 15 million calls to the
distance function for my input, which is rather a lot: thank
goodness I'm not in Python or Matlab or something.

Part 2 is one of those AoC problems, like some in previous
years, where a bound is an implicit input to the
calculation: the example and the input require different
bounds. This is slightly annoying, since the natural way to
handle it requires passing an extra command-line argument
which then has to be scripted-in everywhere.

Things I learned about Javascript:

* The "spread operator" `...`
* The existence of Javascript's functional operators such as
  `.map()` and `.split()`
* The use of labeled statements in Javascript for `break`
  and `continue`.
* That the Javascript `arguments` array is reset to the
  arguments of each called function.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 10000 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
