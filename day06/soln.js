// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 6.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

function parse_coords(lines) {
    return lines.map(cs => cs.split(", ").map(c => parseInt(c)));
}

function find_extrema(coords) {
    const x_coords = coords.map(([x,]) => x);
    const y_coords = coords.map(([,y]) => y);
    const x_min = Math.min(...x_coords);
    const x_max = Math.max(...x_coords);
    const y_min = Math.min(...y_coords);
    const y_max = Math.max(...y_coords);
    return [[x_min, x_max], [y_min, y_max]];
}

function dist([x1, y1], [x2, y2]) {
    const dx = Math.abs(x1 - x2);
    const dy = Math.abs(y1 - y2);
    return dx + dy;
}

function at_box_edge([x, y], [x_bounds, y_bounds]) {
    const c_at = (z, [z_min, z_max]) => z === z_min || z === c_max;
    return c_at(x, x_bounds) || c_at(y, y_bounds);
}

// Strategy: Points that can reach the edge of the
// bounding box defined by the extremal points are the
// ones with infinite area: let's call these escaping
// points[*]. We will compute the closest point P to
// each point inside this bounding box, and assign that
// point to the total area of P, marking the points
// along the edge of the bounding box as escaping.  We
// will then find the maximum area of a contained
// (non-escaping) point.

// [*] See my comment on Reddit
// <https://www.reddit.com/r/adventofcode/comments/a3kr4r/2018_day_6_solutions/eb7axrw/>
// for an explanation of my original buggy solution and
// a proof sketch that the current solution is correct.
function part1(lines) {
    const coords = parse_coords(lines);

    // Get extremal coordinates.
    const extrema = find_extrema(coords);
    const [[x_min, x_max], [y_min, y_max]] = extrema;

    // Compute areas and find escaping points.
    let areas = new Array(coords.length).fill(0);
    // XXX Cleaner to use a Set here, but this
    // representation might be faster.
    let escapes = new Array(coords.length).fill(false);
    for (let j = y_min; j <= y_max; j++) {
        for (let i = x_min; i <= x_max; i++) {
            const c = [i, j];
            let k_min = 0;
            let d_min = dist(c, coords[0]);
            for (let k = 1; k < coords.length; k++) {
                const d = dist(c, coords[k]);
                if (d < d_min) {
                    d_min = d;
                    k_min = k;
                } else if (d === d_min) {
                    k_min = null;
                }
            }
            if (k_min !== null) {
                areas[k_min]++;
                if (at_box_edge(c, extrema))
                    escapes[k_min] = true;
            }
        }
    }

    // Clip out areas of escaping points.
    let areas_contained = [];
    for (let k = 0; k < coords.length; k++)
        if (!escapes[k])
            areas_contained.push(areas[k]);

    print(Math.max(...areas_contained));
}

// Strategy: establish a bound on the possible coordinates
// by noting that points too far off will have a total
// distance greater than bound * coords.length. We loosen
// the bound by just considering each coordinate separately,
// which only adds a factor of two or so to the area
// considered. We then check everything in the bounded area
// and return the sum.
function part2(lines) {
    const BOUND = get_numeric_argument(0, "bound");
    const coords = parse_coords(lines);
    const [[x_min, x_max], [y_min, y_max]] = find_extrema(coords);

    const delta = Math.ceil(BOUND / coords.length) + 1;
    let area = 0;
    for (let j = y_min - delta; j <= y_max + delta; j++) {
        next_i:
        for (let i = x_min - delta; i <= x_max + delta; i++) {
            const c = [i, j];
            let d = dist(c, coords[0]);
            if (d >= BOUND)
                continue;
            for (let k = 1; k < coords.length; k++) {
                d += dist(c, coords[k]);
                if (d >= BOUND)
                    continue next_i;
            }
            area++;
        }
    }

    print(area);
}

run_part();
