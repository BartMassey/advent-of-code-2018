#!/bin/sh
D8=false
case "$1" in
    d8) D8=true ;;
esac
for D in day??
do
    echo $D
    ( cd $D
      if [ $D = day25 ]
      then
          echo -n "sole part: "
          ( /usr/bin/time -f '%e' sh -c "d8 soln.js <input.txt" ) 2>&1
          continue
      fi
      for PART in 1 2
      do
          echo -n "part $PART: "
          egrep "^  *smjs soln.js $PART" README.md |
          if $D8
          then
              sed 's/smjs soln.js/d8 soln.js --/'
          else
              cat
          fi |
          ( read CMD; /usr/bin/time -f '%e' sh -c "$CMD" ) 2>&1 >/dev/null
      done )
done
