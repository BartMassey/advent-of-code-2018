// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 13.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const UP = Symbol("UP");
const DOWN = Symbol("DOWN");
const LEFT = Symbol("LEFT");
const RIGHT = Symbol("RIGHT");
const STRAIGHT = Symbol("STRAIGHT");

// Change in position based on direction.
const DELTA = {
    [UP]: [-1, 0],
    [DOWN]: [1, 0],
    [LEFT]: [0, -1],
    [RIGHT]: [0, 1],
};

// Change in facing for right turn.
const TURN_RIGHT = {
    [UP]: RIGHT,
    [RIGHT]: DOWN,
    [DOWN]: LEFT,
    [LEFT]: UP,
};

// Change in facing for left turn.
const TURN_LEFT = {
    [UP]: LEFT,
    [RIGHT]: UP,
    [DOWN]: RIGHT,
    [LEFT]: DOWN,
};

// Turn state machine.
const NEXT_TURN = {
    [LEFT]: STRAIGHT,
    [STRAIGHT]: RIGHT,
    [RIGHT]: LEFT,
};

// The minus-corner character '\' causes the cart to corner.
const CORNER_MINUS = {
    [RIGHT]: DOWN,
    [DOWN]: RIGHT,
    [LEFT]: UP,
    [UP]: LEFT,
};

// The plus-corner character '/' causes the cart to corner.
const CORNER_PLUS = {
    [RIGHT]: UP,
    [UP]: RIGHT,
    [LEFT]: DOWN,
    [DOWN]: LEFT,
};

// A cart, with an id (null to indicate a removed cart),
// a facing to indicate current travel direction, and a
// turn to indicate where to go at the next junction.
function Cart(id, row, col, facing) {
    this.id = id;
    this.row = row;
    this.col = col;
    this.facing = facing;
    this.turn = LEFT;

    // Roll the cart forward one step, making turns as
    // needed. Return the cart object of the hit cart on
    // collision, or null otherwise. If this cart's id is
    // null, don't roll the cart. Treat collisions with
    // carts of cart id null as non-collisions.
    this.roll = function(tracks, carts) {
        // Possibly adjust facing.
        switch (tracks[this.row][this.col]) {
        case '\\':
            this.facing = CORNER_MINUS[this.facing];
            break;
        case '/':
            this.facing = CORNER_PLUS[this.facing];
            break;
        case '-':
            if (this.facing !== RIGHT && this.facing !== LEFT)
                throw new Error("perpendicular in track");
            break;
        case '|':
            if (this.facing !== UP && this.facing !== DOWN)
                throw new Error("perpendicular in track");
            break;
        case '+':
            switch (this.turn) {
            case LEFT: this.facing = TURN_LEFT[this.facing]; break;
            case STRAIGHT: break;
            case RIGHT: this.facing = TURN_RIGHT[this.facing]; break;
            }
            this.turn = NEXT_TURN[this.turn];
            break;
        case 'v':
        case '^':
        case '>':
        case '<':
            // Implied straight track under cart?
            break;
        default:
            throw new Error("unexpected track symbol");
        }

        // Move the cart.
        const [drow, dcol] = DELTA[this.facing];
        this.row += drow;
        this.col += dcol;

        // Check for collisions.
        for (const c of carts) {
            if (c.id === null)
                continue;
            if (c.id === this.id)
                continue;
            if (c.row === this.row && c.col === this.col)
                return c;
        }
        return null;
    }

    return this;
}

// Find the initial positions and facings of the carts,
// and return a new array of cart objects. After this,
// the cart symbols on the tracks will be ignored.
//
// XXX There are literal corner cases in this program if the
// initial position of a cart was implicitly at a corner or
// intersection. The current code doesn't believe this can
// happen and its behavior is undefined in this case.
function find_carts(tracks) {
    let carts = [];
    for (let row = 0; row < tracks.length; row++) {
        for (let col = 0; col < tracks[row].length; col++) {
            const id = carts.length + 1;
            switch (tracks[row][col]) {
            case '^': carts.push(new Cart(id, row, col, UP)); break;
            case '>': carts.push(new Cart(id, row, col, RIGHT)); break;
            case 'v': carts.push(new Cart(id, row, col, DOWN)); break;
            case '<': carts.push(new Cart(id, row, col, LEFT)); break;
            }
        }
    }
    return carts;
}

// Use specified order for moving carts.
function cmp_cart_posn(c1, c2) {
    let d_row = c1.row - c2.row;
    if (d_row !== 0)
        return d_row;
    return c1.col - c2.col;
}

// Run the overall simulation. If remove_crashes is false,
// the simulation stops at the first crash.  If true, it
// stops when only one cart remains.  In either case, this
// function will display the relevant coordinates.
function run_carts(tracks, remove_crashes) {
    let carts = find_carts(tracks);
    let n_carts = carts.length;

    // Run one simulation step.
    for (let t = 0; true; t++) {
        // Establish current move order within turn.
        carts.sort(cmp_cart_posn);
        for (const c of carts) {
            if (c.id === null)
                continue;
            
            // Move the cart and deal with any collision.
            // XXX If remove_crashes is true, don't actually
            // delete collided carts from the carts list
            // now, as that will create iterator
            // invalidation adventures.  Instead mark the
            // offending carts for deletion after movement
            // is complete by setting their id to null.
            const c2 = c.roll(tracks, carts);
            if (c2 !== null) {
                if (remove_crashes) {
                    c.id = null;
                    c2.id = null;
                    n_carts -= 2;
                } else {
                    print(`${c.col},${c.row}`);
                    return;
                }
            }
        }

        if (remove_crashes) {
            // Actually remove the carts one at a time,
            // using some careful coding to avoid iterator
            // invalidations.
            let n = carts.length;
            while (n > n_carts) {
                for (let i = 0; i < n; i++) {
                    if (carts[i].id === null) {
                        carts.splice(i, 1);
                        n--;
                        break;
                    }
                }
            }
            if (n_carts < 1)
                throw new Error("out of carts");

            // If we are down to 1 cart, we are done.
            if (n_carts === 1) {
                const c = carts[0];
                print(`${c.col},${c.row}`);
                return;
            }
        }
    }
}

function part1(tracks) {
    run_carts(tracks, false);
}

function part2(tracks) {
    run_carts(tracks, true);
}

run_part();
