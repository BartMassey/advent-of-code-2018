# Advent of Code 2018: Day 13
Copyright (c) 2018 Bart Massey

Today's thing is just a straight simulation. Easy algorithm,
but tedious and full of traps.

Things I learned about Javascript:

* How to use the Symbol type to do enum-like things in a
  clean and clear way.

* To be careful of iterator invalidations. (Fortunately I
  was looking out for them.)

* How to delete an element from an array (presumably in a
  computationally-expensive fashion) using the `splice()`
  method.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
