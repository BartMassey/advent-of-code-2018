# Advent of Code 2018: Day 16
Copyright (c) 2018 Bart Massey

Little straightforward build-an-assembler exercise with a
twist.

Took a lot of debugging time, because Javascript. No tests
today, which is a little sad. It would be hard to write a
small test, but it made debugging harder.

Things I learned about Javascript:

* How to use Javascript's `class` to do OOP, with
  setters and getters and stuff.

* Don't name a method `.eval()`, because if you forget to
  call it on an object you will get the builtin function
  of that name instead. This being Javascript, bad arguments
  to that builtin won't cause a crash or anything: it will
  just keep going.

* Embraced `.forEach()` for iterating over index-value pairs
  in order.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
