// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 16.

"use strict";

const TRACING = false;

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const REG = Symbol("reg");
const IMM = Symbol("imm");
const IGN = Symbol("ign");

class Arg {
    constructor(letter, posn) {
        this.letter = letter;
        this.posn = posn;
        switch (letter) {
        case '-': this.type = IGN; break;
        case 'r': this.type = REG; break;
        case 'i': this.type = IMM; break;
        default: throw new Error("unknown arg type: " + letter);
        }
        return this;
    }

    get() {
        return this.type;
    }

    reg(regs) {
        return regs[this.posn];
    }

    toString() {
        return this.letter;
    }
}

let opcode_table = new Array();

class Op {
    constructor(name, args, op) {
        this.name = name;
        this._opcode = null;
        this.args = args.split('').map((l, i) => new Arg(l, i));
        this.op = op;
        return this;
    }

    exec(opds, regs) {
        trace("exec", this.name, ...opds);
        trace(" before", regs);
        const opargs = [];
        this.args.forEach((a, i) => {
            switch (a.type) {
            case IMM: opargs.push(opds[i]); break;
            case REG: {
                if (opds[i] < 0 || opds[i] >= 4)
                    throw new Error("bad register spec");
                opargs.push(regs[opds[i]]);
                break; }
            case IGN: break;
            default: throw new Error("bad argument type");
            }
        });
        regs[opds[opds.length - 1]] = this.op(...opargs);
        trace(" after", regs);
    }

    get opcode() {
        if (this._opcode === null)
            throw new Error("fetched undefined opcode for " + this.name);
        return this._opcode;
    }

    set opcode(v) {
        this._opcode = v;
        opcode_table[v] = this;
    }

    toString() {
        return this.name + " " + this.args.join(' ');
    }
}

const ops = [
    new Op("addr", "rr", (x, y) => x + y),
    new Op("addi", "ri", (x, y) => x + y),
    new Op("mulr", "rr", (x, y) => x * y),
    new Op("muli", "ri", (x, y) => x * y),
    new Op("banr", "rr", (x, y) => x & y),
    new Op("bani", "ri", (x, y) => x & y),
    new Op("borr", "rr", (x, y) => x | y),
    new Op("bori", "ri", (x, y) => x | y),
    new Op("setr", "r-", (x) => x),
    new Op("seti", "i-", (x) => x),
    new Op("gtir", "ir", (x, y) => +(x > y)),
    new Op("gtri", "ri", (x, y) => +(x > y)),
    new Op("gtrr", "rr", (x, y) => +(x > y)),
    new Op("eqir", "ir", (x, y) => +(x === y)),
    new Op("eqri", "ri", (x, y) => +(x === y)),
    new Op("eqrr", "rr", (x, y) => +(x === y)),
];

class Regs {
    constructor(...regs) {
        this.regs = regs.slice();
        return this;
    }

    get() {
        return this.regs;
    }

    equals(r) {
        for (let i = 0; i < r.length; i++)
            if (this.regs[i] !== r[i])
                return false;
        return true;
    }
    
    toString() {
        return "[" + this + "]";
    }
}

class Insn {
    constructor(line) {
        let insn = line.split(' ').map(s => parseInt(s));
        this.opcode = insn[0];
        this.opds = insn.slice(1);
        this.name = null;
        return this;
    }

    toString() {
        return this.opcode + ' ' + this.opds.join(' ');
    }
}

class Sample {
    constructor(lines) {
        const [before, insn, after] = lines.split('\n');
        
        function get_regs(spec, field) {
            const m = field.match(spec);
            if (m === null)
                throw new Error("match failed: \"" + field + "\", " + spec);
            return new Regs(...m[1].split(", ").map(s => parseInt(s)));
        }
        
        this.before = get_regs(/^Before: *\[(.*)\]$/, before);
        this.insn = new Insn(insn);
        this.after = get_regs(/^After: *\[(.*)\]$/, after);
    }

    toString() {
        return [
            this.before.toString(),
            this.insn.toString(),
            this.after.toString(),
        ].join('\n');
    }
}

function read_spec(lines) {
    const text = lines.join('\n');
    let [samples, program] = text.split(/\n\n\n+/);
    samples = samples.split('\n\n').map(s => new Sample(s));
    program = program.split('\n').map(s => new Insn(s));
    return [samples, program];
}

function sequence(n) {
    let result = [];
    for (let i = 0; i < n; i++)
        result.push(i);
    return result;
}

function decode(samples) {
    let ok_opcodes = sequence(ops.length)
        .map(_ => new Set([...sequence(ops.length)]));
    let nok = 0;
    samples.forEach((sample, j) => {
        let ok = new Set([...sequence(ops.length)]);
        ops.forEach((op, i) => {
            let regs = sample.before.regs.slice();
            op.exec(sample.insn.opds, regs);
            if (sample.after.equals(regs)) {
                // trace("insn", i, "ok for", j);
            } else {
                // trace("insn", i, "fails", j);
                ok.delete(i);
                ok_opcodes[i].delete(sample.insn.opcode);
            }
        });
        if (ok.size >= 3) {
            // trace("accepted", j);
            nok++;
        }
    });

    let running = true;
    while (running) {
        running = false;
        for (const i in ok_opcodes) {
            const v = ok_opcodes[i];
            if (v.size === 0)
                continue;
            if (v.size === 1) {
                const opcode = [...v][0];
                ops[i].opcode = opcode;
                for (const xv of ok_opcodes)
                    xv.delete(opcode);
                running = true;
            }
        }
    }

    return nok;
}

function part1(lines) {
    const [samples] = read_spec(lines);
    print(decode(samples));
}

function run(program) {
    let regs = [0, 0, 0, 0];
    for (const insn of program) {
        const op = opcode_table[insn.opcode];
        trace("-", op.name, ...insn.opds);
        op.exec(insn.opds, regs);
    }
    return regs;
}

function part2(lines) {
    const [samples, program] = read_spec(lines);
    decode(samples);
    // for (const v of ops)
    //     trace(opcode_table[v.opcode] + ":", v.opcode);

    const regs = run(program);
    print(regs[0]);
}

run_part();
