// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 15.

"use strict";

load('../lib/aoc.js');

const TRACING = false;
    
let attack_power = {
    'E': 3,
    'G': 3,
};

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

function opponent(label) {
    if (label === 'E')
        return 'G';
    if (label === 'G')
        return 'E';
    return null;
}

function Square(label) {
    this.label = label;
    this.opponent = opponent(label);

    this.toString = function() { return this.label; }

    if (this.opponent !== null)
        this.hp = 200;

    return this;
}

function Board(lines) {
    this.nr = lines.length;
    this.nc = lines[0].length;
    this.squares = make_2d_array(
        this.nr, this.nc, (r, c) => new Square(lines[r][c]));

    this.toString = function() {
        let result = "";
        let annotations = [];
        for (const [s,, c] of this.squares) {
            result += s.label;
            if (s.opponent !== null)
                annotations.push(s.label + "(" + s.hp + ")");
            if (c === this.nc - 1) {
                if (annotations.length > 0)
                    result += "    " + annotations.join(", ");
                result += '\n';
                annotations = [];
            }
        }
        return result;
    }

    this.count_creatures = function(label) {
        let count = 0;
        for (const [s] of this.squares)
            if (s.label === label && s.hp > 0)
                count++;
        return count;
    }

    this.neighbors = function(r, c) {
        let result = [];

        if (r - 1 >= 0)
            result.push([r - 1, c]);
        if (c - 1 >= 0)
            result.push([r, c - 1]);
        if (c + 1 < this.nc)
            result.push([r, c + 1]);
        if (r + 1 < this.nr)
            result.push([r + 1, c]);

        for (let n of result) {
            const [xr, xc] = n;
            n.push(this.squares[xr][xc]);
        }

        return result;
    }

    this.foe_paths = function(s0, r0, c0) {
        let open = new Queue();
        let closed = new Set();
        let paths = [];
        open.push([s0, r0, c0, []]);
        closed.add(r0 + "," + c0);
        while (true) {
            const next = open.pop();
            if (next === null)
                break;
            const [s, r, c, p] = next;
            if (s.label === s0.opponent)
                paths.push([s, p]);
            for (const [xr, xc, xs] of this.neighbors(r, c)) {
                if (closed.has(xr + "," + xc))
                    continue;
                if (xs.label !== '.' && xs.label !== s0.opponent)
                    continue;
                let xp = p.slice();
                xp.push([xr, xc]);
                const neighbor = [xs, xr, xc, xp];
                open.push(neighbor);
                closed.add(xr + "," + xc);
            }
        }
        return paths;
    }

    this.run_action = function(s, r, c) {
        trace("action for", s, r, c)

        // Check for termination. XXX Stupidly expensive.
        if (this.count_creatures(s.opponent) === 0)
            return false;

        // Do pathfinding.
        let foes;
        let adjacencies;
        const update_state = () => {
            foes = this.foe_paths(s, r, c);
            adjacencies = foes.filter(([,p]) => p.length === 1);
        };
        update_state();

        // Move if necessary.
        if (adjacencies.length === 0) {
            if (foes.length === 0) {
                trace("move: cannot move");
            } else {
                // https://www.reddit.com/r/adventofcode/comments/
                // a6yleq/2018_day_15_part_2_my_solution_seems_to_work_for/
                // ebyz4rd/
                const [, p] = foes[0];
                let nearest_foes =
                    foes.filter(([,xp]) => xp.length === p.length);
                const dict_order = (p1, p2) =>
                      p1[0] === p2[0] ? p1[1] - p2[1] : p1[0] - p2[0];
                const foe_loc = ([,p]) => p[p.length - 1];
                const foe_order = (f1, f2) =>
                      dict_order(foe_loc(f1), foe_loc(f2));
                nearest_foes.sort(foe_order);
                const [, xp] = nearest_foes[0];
                const [xr, xc] = xp[0];

                trace("move", r, c, xr, xc);
                this.squares[xr][xc] = s;
                this.squares[r][c] = new Square('.');
                r = xr;
                c = xc;
                update_state();
            }
        }

        // Attack if possible.
        if (adjacencies.length > 0) {
            let target = null;
            for (const t of adjacencies) {
                // XXX t[0] is foe
                const is_min =
                    t[0].hp > 0 &&
                    (target === null || t[0].hp < target[0].hp);
                if (is_min) 
                    target = t;
            }

            if (target !== null) {
                const [foe, foe_path] = target;
                const [xr, xc] = foe_path[0];
                trace("attack", r, c, xr, xc, foe.label);

                foe.hp -= attack_power[s.label];
                if (foe.hp <= 0)
                    this.squares[xr][xc] = new Square('.');
            }
        }

        return true;
    }

    this.run_round = function() {
        let creatures = [...this.squares]
            .filter(([s]) => s.opponent !== null);
        for (const [s, r, c] of creatures) {
            // XXX Watch for zombies!
            if (s.hp <= 0)
                continue;
            if (!this.run_action(s, r, c))
                return s.label;
        }

        return null;
    }

    this.sum_hitpoints = function(label) {
        let t = 0;
        for (const [s] of this.squares)
            if (s.label === label)
                t += s.hp;
        return t;
    }

    this.run_battle = function() {
        let round = 0;
        while (true) {
            trace("---", round);
            trace(this);
            const winner = this.run_round();
            if (winner !== null) {
                const hp = this.sum_hitpoints(winner);
                trace("===", round);
                trace(this);
                return [winner, round, hp];
            }
            round++;
        }
        throw new Error("battle ended early");
    }

    return this;
}


function part1(lines) {
    let board = new Board(lines);
    const [winner, rounds, hp] = board.run_battle();
    print(rounds * hp);
}

function part2(lines) {
    for (attack_power['E'] = 4; true; attack_power['E']++) {
        let board = new Board(lines);
        let n_elves = board.count_creatures('E');
        const [winner, rounds, hp] = board.run_battle();
        if (winner === 'E' && board.count_creatures('E') === n_elves) {
            print(rounds * hp);
            return;
        }
    }
}

run_part();
