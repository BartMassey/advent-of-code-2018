#!/usr/bin/python3

from sys import stdin, argv

f_input = open(argv[1] + "-input.txt", "w")
f_output = open(argv[1] + "-output.txt", "w")

start = next(stdin)
width = start.index(' ')
output_start = width + start[width:].index('#')

def split_line(line):
    line = line.rstrip()
    print(line[:width], file=f_input)
    print(line[output_start:], file=f_output)
    
split_line(start)
for line in stdin:
    split_line(line)
