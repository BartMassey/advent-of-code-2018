# Advent of Code 2018: Day 15
Copyright (c) 2018 Bart Massey

Well that was a meatgrinder.

The simulation is always the toughest AoC problem, but this
one was tougher for me than usual. The rules are *very*
complicated, and I botched them several times. Also,
debugging Javascript is so hard.

Honestly, I almost gave up. A Redditor saved me at the end
of Part 2 with
[this comment](https://www.reddit.com/r/adventofcode/comments/a6yleq/2018_day_15_part_2_my_solution_seems_to_work_for/ebyz4rd/).
Honestly, I don't know if I would have gotten it otherwise.

My solution is quite slow: 5s for Part 1 and 48s for Part 2
on my box. I could binary-search for the needed attack power
and probably save quite a bit of that. Mostly I need to
implement more efficient pathing and action processing.

Things I learned about Javascript:

* How to implement my own `.toString()` methods properly.

* More Javascript OOP.

* Javascript iterators and generators.

* How to check whether a global variable exists.

* How to hack up an Array of Arrays to act kind of like a 2D
  Array.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
