# Advent of Code 2018: Day 14
Copyright (c) 2018 Bart Massey

Spent a bunch of time guessing the wrong generalization for
Part 2 and debugging badly. Ended up with pretty versatile
and readable code, but not much performance. Part 2 runs in
17s on my laptop, by far my slowest effort so far. I'm sure
the Internets will tell me how to speed it.

Things I learned about Javascript:

* There is no standard `zip()` method or function for
  arrays, and the DIY one looks gross.

* Optional arguments to the mapped function don't work right
  with `map()`.

        > ["1", "2"].map(parseInt)
        [1, NaN]
        > ["1", "2"].map(c=>parseInt(c))
        [1, 2]

  …Oh. After further investigation, I am reminded that
  Javascript `map()` passes a whole bunch of cruft to the
  mapping function: the second argument is the index.  These
  extra arguments are simply *thrown away* if not used, but if
  passes to a bare function will supply values to the
  default arguments. The second optional argument to
  `parseInt()` is the number base. On both `smjs` and `d8`
  `parseInt()` treats base 0 as base 10; it refuses to
  encode in base 1.  So sure enough we get "1" converted to
  base 10 and then "2" not converted.

  Have I mentioned lately how much I hate Javascript?

Run with

    smjs soln.js 1 540391
    smjs soln.js 2 540391

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
