// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 14.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

// Sum elements of an array.
const sum = a => a.reduce((s, v) => s + v, 0);

// Turn a recipe number or string into an array of digits.
const recipize = r =>
    [...r.toString()].map(c => parseInt(c));

// Run the cooking loop until cont(recipes) returns false,
// then return all the recipes created.
function cook(cont) {
    let recipes = [3, 7];
    let elves = [0, 1];
    while(true) {
        const newRecipes =
              recipize(sum(elves.map(i => recipes[i])));
        for (const r of newRecipes) {
            if (cont(recipes))
                recipes.push(r);
            else
                return recipes;
        }
        elves = elves.map(i => (i + 1 + recipes[i]) % recipes.length);
    }
}

// Turn an array of digits into its string representation.
const stringify = a => a.map(r => r.toString()).join('');

function part1() {
    const n_trials = get_numeric_argument(0, "trials");
    const cont = recipes => recipes.length < n_trials + 10;
    const recipes = cook(cont);
    print(stringify(recipes.slice(-10)));
}

function part2() {
    const target = get_argument(0, "target");
    const cont = recipes =>
          stringify(recipes.slice(-target.length)) !== target;
    const recipes = cook(cont);
    print(recipes.length - target.length);
}

run_part(false);
