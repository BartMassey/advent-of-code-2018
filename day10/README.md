# Advent of Code 2018: Day 10
Copyright (c) 2018 Bart Massey

Huh. ASCII graphics. That's supposed to be one of my
specialties.

Took me a bit to realize that I had to watch for the minimum
bounding box. Then it was all just bugs, most of them
Javascript bugs. I was pleased that I had presciently
calculated and displayed the Part 2 answer in my Part 1
implementation.

There is definitely a faster algorithm available here.  The
obvious plan involves minimizing a set of simultaneous
vector equations somehow, probably by linear programming. My
solution runs in about 1.7s on my box: that's fast enough.

Things I learned about Javascript:

* As far as I can tell, there's no standard way in the core
  language to deep-copy a thing. This is *really* painful.
  I ended up implementing a dumb copy constructor.

* As far as I can tell, there's no Javascript way to have a
  `Set` of anything but primitive objects. The fact that
  `[1,2] != [1, 2]` continues to haunt me. I ended up making
  a set of strings, because I couldn't find a grosser way.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

although the latter just tells you to run the former.

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
