// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 10.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const POINT_RE =
    /position=< *(-?\d+),  *(-?\d+)> velocity=< *(-?\d+),  *(-?\d+)>/;

function move_point() {
    this.x += this.dx;
    this.y += this.dy;
}

// XXX Copy method for Point, because thanks Javascript.
function clone() {
    let c = new Object();
    c.x = this.x;
    c.y = this.y;
    c.dx = this.dx;
    c.dy = this.dy;

    c.move_point = move_point;
    c.clone = clone;
    return c;
}

// Make a new point from the pattern match.
function Point(m) {
    const [, x, y, dx, dy] = m;
    this.x = +x;
    this.y = +y;
    this.dx = +dx;
    this.dy = +dy;

    this.move_point = move_point;
    this.clone = clone;
    return this;
}

// XXX Keep track of last width and height seen.  This is a
// bad idea, and should be incorporated into the return of
// find_bbox().
let last_width = null;
let last_height;

function find_bbox(points) {
    // Find bounds.
    const xs = points.map(p => p.x);
    const x_min = Math.min(...xs);
    const x_max = Math.max(...xs);
    const ys = points.map(p => p.y);
    const y_min = Math.min(...ys);
    const y_max = Math.max(...ys);

    // Update saved width and height.
    const width = x_max - x_min;
    const height = y_max - y_min;
    if (last_width !== null) {
        if (last_width < width || last_height < height)
            return null;
    }
    last_width = width;
    last_height = height;

    // Return bbox.
    return [[x_min, x_max], [y_min, y_max]];
}

// Strategy: Put the points in a set, and iterate over the
// bounding box, checking to see which points are in the
// set.
function show_points(points, [[x_min, x_max], [y_min, y_max]]) {
    // Build the set. XXX Note that it has to be a set of
    // strings, because thanks Javascript.
    let posns = new Set(points.map(p => p.x + "," + p.y));
    for (let y = y_min; y <= y_max; y++) {
        let line = [];
        for (let x = x_min; x <= x_max; x++) {
            if (posns.has(x + "," + y))
                line.push("*");
            else
                line.push(".");
        }
        print(line.join(''));
    }
}

function parse_points(lines) {
    return lines.map(l => new Point(l.match(POINT_RE)));
}


function clone_points(points) {
    return points.map(p => p.clone());
}

function move_points(points) {
    for (const p of points)
        p.move_point();
}

// Strategy: Move the points until their bounding box gets
// bigger rather than smaller. Then print the previous
// position of the points and the time it took to get there.
function part1(lines) {
    // Set up.
    let points = parse_points(lines);
    find_bbox(points);
    let last_points = clone_points(points);

    // Run iterations.
    let t = 1;
    while (true) {
        move_points(points);
        const bbox = find_bbox(points);
        if (bbox === null) {
            const last_bbox = find_bbox(last_points);
            print(t - 1);
            show_points(last_points, last_bbox);
            return;
        }
        last_points = clone_points(points);
        t++;
    }

    // This should be unreachable.
    throw new Error("internal error: never grew");
}

function part2(lines) {
    print("run part 1 for part 2 solution");
}

run_part();
