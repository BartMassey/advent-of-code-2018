// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 11.

"use strict";

function power_level(x, y, gsn) {
    const rack_id = x + 11;
    const base_power = rack_id * (y + 1) + gsn;
    const mult_power = rack_id * base_power;
    // XXX https://stackoverflow.com/questions/4228356/
    const digit_power = (~~(mult_power / 100)) % 10;
    const result_power = digit_power - 5;
    return result_power;
}

function make_2d_array(dim) {
    let result = new Array(dim);
    for (let x = 0; x < dim; x++)
        result[x] = new Array(dim);
    return result;
}

const GRID_SIZE = 300;

let power;

function fill_power(gsn) {
    power = make_2d_array(GRID_SIZE);
    for (let x = 0; x < GRID_SIZE; x++)
        for (let y = 0; y < GRID_SIZE; y++)
            power[x][y] = power_level(x, y, gsn);
}

let totals;

function make_totals(g) {
    let x_totals = make_2d_array(GRID_SIZE);
    for (let y = 0; y < GRID_SIZE; y++) {
        let t = 0;
        for (let x = 0; x < g; x++)
            t += power[x][y];
        x_totals[0][y] = t;
        for (let x = 1; x <= GRID_SIZE - g; x++) {
            let t = x_totals[x - 1][y];
            t -= power[x - 1][y];
            t += power[x + g - 1][y];
            x_totals[x][y] = t;
        }
    }

    totals = make_2d_array(GRID_SIZE - g + 1);
    for (let x = 0; x <= GRID_SIZE - g; x++) {
        let t = 0;
        for (let y = 0; y < g; y++)
            t += x_totals[x][y];
        totals[x][0] = t;
        for (let y = 1; y <= GRID_SIZE - g; y++) {
            let t = totals[x][y - 1];
            t -= x_totals[x][y - 1];
            t += x_totals[x][y + g - 1];
            totals[x][y] = t;
        }
    }
}

function power_by_grid(g) {
    make_totals(g);

    let t_max = totals[0][0];
    let c_max = [0, 0];
    for (let x = 0; x <= GRID_SIZE - g; x++) {
        for (let y = 0; y <= GRID_SIZE - g; y++) {
            const t = totals[x][y];
            if (t_max < t) {
                c_max = [x, y];
                t_max = t;
            }
        }
    }
    return [t_max, c_max];
}


function part1(lines) {
    const gsn = 5235;
    fill_power(gsn);
    const [,[x, y]] = power_by_grid(3);
    print(`${x + 1},${y + 1}`);
}

function part2(lines) {
    const gsn = 5235;
    fill_power(gsn);

    let g_max = 1;
    let [t_max, c_max] = power_by_grid(1, gsn);
    for (let g = 2; g <= GRID_SIZE; g++) {
        let [t, c] = power_by_grid(g, gsn);
        if (t > t_max) {
            t_max = t;
            c_max = c;
            g_max = g;
        }
    }

    const [x, y] = c_max;
    print(`${x + 1},${y + 1},${g_max}`);
}

part2();
