// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 11.

fn power_level(x: usize, y: usize, gsn: usize)  -> i64 {
    let rack_id = x as i64 + 11;
    let base_power = rack_id * (y as i64 + 1) + gsn as i64;
    let mult_power = rack_id * base_power;
    let digit_power = (mult_power / 100) % 10;
    let result_power = digit_power - 5;
    result_power
}

fn make_2d_array(dim: usize) -> Vec<Vec<i64>> {
    let mut result = Vec::with_capacity(dim);
    for x in 0 .. dim {
        result.push(Vec::with_capacity(dim));
        for _ in 0 .. dim {
            result[x].push(0);
        }
    }
    result
}

const GRID_SIZE: usize = 300;

fn find_power(gsn: usize) -> Vec<Vec<i64>> {
    let mut power = make_2d_array(GRID_SIZE);
    for x in 0 .. GRID_SIZE {
        for y in 0 .. GRID_SIZE {
            power[x][y] = power_level(x, y, gsn);
        }
    }
    power
}

fn make_totals(power: &[Vec<i64>], g: usize) -> Vec<Vec<i64>> {
    let mut x_totals = make_2d_array(GRID_SIZE);
    for y in 0 .. GRID_SIZE {
        let mut t = 0;
        for x in 0..g {
            t += power[x][y];
        }
        x_totals[0][y] = t;
        for x in 1 ..= GRID_SIZE - g {
            let mut t = x_totals[x - 1][y];
            t -= power[x - 1][y];
            t += power[x + g - 1][y];
            x_totals[x][y] = t;
        }
    }

    let mut totals = make_2d_array(GRID_SIZE - g + 1);
    for x in 0 ..= GRID_SIZE - g {
        let mut t = 0;
        for y in 0 .. g {
            t += x_totals[x][y];
        }
        totals[x][0] = t;
        for y in 1 ..= GRID_SIZE - g {
            let mut t = totals[x][y - 1];
            t -= x_totals[x][y - 1];
            t += x_totals[x][y + g - 1];
            totals[x][y] = t;
        }
    }
    totals
}

fn power_by_grid(power: &[Vec<i64>], g: usize) -> (i64, (usize, usize)) {
    let totals = make_totals(power, g);

    let mut t_max = totals[0][0];
    let mut c_max = (0, 0);
    for x in 0 ..= GRID_SIZE - g {
        for y in 0 ..= GRID_SIZE - g {
            let t = totals[x][y];
            if t_max < t {
                c_max = (x, y);
                t_max = t;
            }
        }
    }
    (t_max, c_max)
}


fn part2() {
    let gsn: usize = std::env::args()
        .nth(1).expect("no gsn").parse().expect("bad gsn");
    let power = find_power(gsn);

    let mut g_max = 1;
    let (mut t_max, mut c_max) = power_by_grid(&power, 1);
    for g in 2 ..= GRID_SIZE {
        let (t, c) = power_by_grid(&power, g);
        if t > t_max {
            t_max = t;
            c_max = c;
            g_max = g;
        }
    }

    let (x, y) = c_max;
    println!("{},{},{}", x + 1, y + 1, g_max);
}

fn main() {
    part2();
}
