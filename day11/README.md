# Advent of Code 2018: Day 11
Copyright (c) 2018 Bart Massey

Maximal subarray sum: OK.  Was hoping to avoid the dynamic
programming approach, but Part 2 pretty much required it for
reasonable efficiency, as expected.

Sadly, my Part 2 solution produced the right answer on both
test problems, but the answer it gave for my actual problem
was rejected. I shamefacedly resorted to asking the
internets for help. Before they were able to do so, I found
the bug by translating my code to Rust, a
reasonably-debuggable language. It was kind enough to panic
appropriately when I was doing something stupid, leading to
a fix after some thought.

A new input format appears: a single parameter as the
problem input.  I modified my library to optionally allow
problems that don't read from standard input, and took the
parameter from the command line.

Things I learned about Javascript:

* As with Python, the obvious 2D array construction

        new Array(300).from(new Array 300)

  produces a bunch of aliases to a single array. Ah well,
  the loop is easy to write.

* Debugging a Javascript program is hopeless. It steadfastly
  refuses to fail when you do something dumb.

Run with

    smjs soln.js 1 5235
    smjs soln.js 2 5235

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
