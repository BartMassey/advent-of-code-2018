# Advent of Code 2018: Day 3
Copyright (c) 2018 Bart Massey

This problem had the first formatted input, which was an
opportunity to learn Javascript's regular expression
support. I also chose to use a Javascript Object to
represent each "patch" in the input, so I got to play with
the object support a bit.

For counting the number of overlaps at a given position I
chose to go with a problem representation I've used before.
Instead of trying to figure out how big an array to build,
or resizing an array on the fly, I construct a hash table
(Javascript Object) of overlap counts keyed by *x-y*
coordinate.

Performance is far from fantastic, but around 1s of realtime
is acceptable.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
