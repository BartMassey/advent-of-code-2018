// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 3.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

const PROBLEM_LINE = new RegExp(
    /#([1-9][0-9]*) @ ([0-9]+),([0-9]+): ([1-9][0-9]*)*x([1-9][0-9]*)*/
);

function Patch(line) {
    let fields = PROBLEM_LINE.exec(line);
    this.id = +fields[1];
    this.x = +fields[2];
    this.y = +fields[3];
    this.w = +fields[4];
    this.h = +fields[5];
    return this;
}

function read_patches(lines) {
    let patches = [];
    for (let i = 0; i < lines.length; i++)
        patches.push(new Patch(lines[i]));
    return patches;
}

function lap_it(patches) {
    let laps = {};
    for (let i = 0; i < patches.length; i++) {
        let p = patches[i];
        for (let j = p.x; j < p.x + p.w; j++) {
            for (let k = p.y; k < p.y + p.h; k++) {
                let coord = [j, k];
                if (coord in laps) {
                    laps[coord]++;
                } else {
                    laps[coord] = 1;
                }
            }
        }
    }
    return laps;
}

function part1(lines) {
    let patches = read_patches(lines);
    let laps = lap_it(patches);
    let nlaps = 0;
    for (let coord in laps) {
        if (laps[coord] > 1) {
            nlaps++;
        }
    }
    print(nlaps);
}

function independent(laps, p) {
    for (let j = p.x; j < p.x + p.w; j++) {
        for (let k = p.y; k < p.y + p.h; k++) {
            let coord = [j, k];
            if (laps[coord] > 1)
                return false;
        }
    }
    return true;
}

function part2(lines) {
    let patches = read_patches(lines);
    let laps = lap_it(patches);
    for (let i = 0; i < patches.length; i++) {
        let p = patches[i];
        if (independent(laps, p)) {
            print(p.id);
            return;
        }
    }
    throw new Error("cannot find nonoverlapping patch");
}

run_part();
