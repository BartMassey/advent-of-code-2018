// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code character processing functions.

function ord(c) {
    return c.codePointAt(0);
}

function chr(c) {
    return String.fromCodePoint(c);
}

function ordn(c) {
    return ord(c.toLowerCase()) - ord('a');
}

function chrn_lower(n) {
    return chr(n + ord('a'));
}

function chrn_upper(n) {
    return chr(n + ord('A'));
}
