// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code common functions.

load('../lib/charcode.js');

function save_arguments(args) {
    if (typeof scriptArgs == 'undefined') {
        if (typeof args == 'undefined')
            throw new Error("cannot find arguments");
        scriptArgs = args;
    }
}

function read_input() {
    let lines = [];
    let ns;
    while ((ns = readline()) !== null)
        lines.push(ns);
    return lines;
}

function run_part(read_lines = true) {
    let lines;
    if (read_lines)
        lines = read_input();
    switch (scriptArgs[0]) {
        case "1": part1(lines); break;
        case "2": part2(lines); break;
        default: throw new Error("unknown part");
    }
}

function get_argument(index, name) {
    index += 1;
    if (scriptArgs.length <= index)
        throw new Error(`missing argument ${index}: <${name}>`);
    return scriptArgs[index];
}

function get_numeric_argument(index, name) {
    const result = +get_argument(index, name);
    if (result === NaN)
        throw new Error(`non-numeric argument ${index + 1}: <${name}>`);
    return result;
}

function make_2d_array(dim_r, dim_c, initializer = null) {
    let result = new Array(dim_r);
    for (let r = 0; r < dim_r; r++) {
        result[r] = new Array(dim_c);
        if (initializer !== null) {
            for (let c = 0; c < dim_c; c++)
                result[r][c] = initializer(r, c);
        }
    }

    result[Symbol.iterator] = function* () {
        for (let r = 0; r < this.length; r++)
            for (let c = 0; c < this[r].length; c++)
                yield [this[r][c], r, c];
    }

    result.toString = function() {
        let result = "";
        for (let r = 0; r < this.length; r++) {
            for (let c = 0; c < this[r].length; c++)
                result += this[r][c];
            result += '\n';
        }
        return result;
    }

    return result;
}

const make_square_array = dim => make_2d_array(dim, dim);

function trace() {
    if (typeof TRACING !== 'boolean')
        throw new Error("trace: no tracing");
    if (TRACING)
        print(...arguments);
}

function Queue() {
    this.head = [];
    this.tail = [];
    this.size = 0;

    this.push = function(v) {
        this.head.push(v);
        this.size++;
    }

    this.pop = function() {
        if (this.size === 0)
            return null;
        if (this.tail.length === 0) {
            this.head.reverse();
            this.tail = this.head;
            this.head = [];
        }
        this.size--;
        return this.tail.pop();
    }
}
