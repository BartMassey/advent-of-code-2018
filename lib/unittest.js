function deepEquals(a, b) {
    if (typeof a !== typeof b)
        return false;
    if (typeof a === "object") {
        const ka = Object.keys(a);
        const kb = Object.keys(b);
        for (const i of ka)
            if (!deepEquals(a[i], b[i]))
                return false;
        for (const i of kb)
            if (!deepEquals(a[i], b[i]))
                return false;
        return true;
    }
    return a === b;
}

function unitTest(f, tests) {
    for (const [t_in, t_expect] of tests) {
        print(typeof t_in);
        let t_out = f(...t_in);
        if (!deepEquals(t_out, t_expect))
            console.log(`${f}(${t_in}): ${t_expect} !== ${t_out}`);
        else
            console.log(`${f.name}(${t_in}): ${t_expect} passed`);
    }
}


unitTest(deepEquals, [
    [[1, 1], true],
    [[1, "hello"], false],
    [[[1, 2], [1, 2]], true],
    [[[1, 2], [1, Number(2)]], true],
]);
