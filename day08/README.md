# Advent of Code 2018: Day 8
Copyright (c) 2018 Bart Massey

As usual, it's the fiddly details that took a while with
this one.

I chose to build an explicit tree object rather than
directly traversing the data. Rather than do a bunch of
gratuitous copying, I just stuck a reference to the whole
array of input values in every node of the tree, and
referred to everything else positionally. This was a
questionable choice for clarity and readability, but was
fast enough.

In Part 2, I missed that children are numbered starting with
1 rather than 0, and also that it is intended to skip child
indices that are out of range. I also had some Javascript
"fun".

Things I learned about Javascript:

* As with Python, `[]` creates a fresh object rather than
  being a constant. Also, `===` really is `eq`. Consequence:
  `[] === []` is `false`. Mysteriously, `[] == []` is also
  `false`, so I guess there's no array comparison in
  Javascript.

* Figured out enough of a Javascript OOP style to write in
  plausibly. Wish I knew how it compares with standard
  practice.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
