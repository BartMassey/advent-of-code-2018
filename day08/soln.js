// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 8.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);



const sum = a => a.reduce((a, b) => a + b, 0);

function Node(values, posn) {
    const n = values.length;
    const nchildren = values[posn];
    const nmeta = values[posn + 1];
    let body = posn + 2;
    this.values = values;

    this.children = [];
    for (let i = 0; i < nchildren; i++) {
        const child = new Node(values, body);
        this.children.push(child);
        body = child.metadata[1];
    }

    this.metadata = [body, body + nmeta];

    this.sum_meta = function() {
        const [start_md, end_md] = this.metadata;
        let t = sum(this.values.slice(start_md, end_md));
        for (const c of this.children)
            t += c.sum_meta();
        return t;
    }

    this.value_tree = function() {
        const [start_md, end_md] = this.metadata;
        const meta = this.values.slice(start_md, end_md);
        if (this.children.length === 0)
            return sum(meta);
        let t = 0;
        for (let c of meta) {
            c--;
            if (c >= this.children.length)
                continue;
            t += this.children[c].value_tree();
        }
        return t;
    }

    return this;
}

const make_tree = lines => new Node(lines[0].split(' ').map(s => +s), 0);

function part1(lines) {
    print(make_tree(lines).sum_meta());
}

function part2(lines) {
    print(make_tree(lines).value_tree());
}

run_part();
