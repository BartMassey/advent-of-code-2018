# Advent of Code 2018: Day 2
Copyright (c) 2018 Bart Massey

Interesting exercise in Javascript looping and string
manipulation.

My part 2 solution uses an *O(n^2 m)* algorithm, where *n*
is the number of labels and *m* is the label length. One can
surely do better, but meh.

Run with

    smjs soln.js 1 <input.txt
    smjs soln.js 2 <input.txt

---

This program is licensed under the "MIT License".
Please see the file LICENSE in this distribution
for license terms.
