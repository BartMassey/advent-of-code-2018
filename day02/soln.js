// Copyright © 2018 Bart Massey
// This program is licensed under the "MIT License".
// Please see the file LICENSE in this distribution
// for license terms.

// Advent of Code Day 2.

"use strict";

load('../lib/aoc.js');

// d8 argument workaround
if (typeof arguments !== 'undefined')
    save_arguments(arguments);

function has_repeat(hist, count) {
    for (let k in hist)
        if (hist[k] == count)
            return true;
    return false;
}

function build_hist(line) {
    let hist = {};
    for (const c of line) {
        if (c in hist) {
            hist[c] += 1;
        } else {
            hist[c] = 1;
        }
    }
    return hist;
}

function part1(lines) {
    let ntwos = 0;
    let nthrees = 0;
    for (const l of lines) {
        let hist = build_hist(l);
        if (has_repeat(hist, 2)) {
            ntwos++;
        }
        if (has_repeat(hist, 3)) {
            nthrees++;
        }
    }
    print(ntwos * nthrees);
}

function diff1(line1, line2) {
    let ndiff = 0;
    let accum = "";
    if (line1.length !== line2.length) {
        throw new Error("diff1: unequal-length lines");
    }
    for (let i = 0; i < line1.length; i++) {
        let c1 = line1[i];
        let c2 = line2[i];
        if (c1 === c2) {
            accum += c1;
        } else {
            ndiff += 1;
            if (ndiff > 1)
                return null;
        }
    }
    if (ndiff === 1) {
        return accum;
    }
    return null;
}

function part2(lines) {
    for (let i = 0; i < lines.length; i++) {
        for (let j = i + 1; j < lines.length; j++) {
            let answer = diff1(lines[i], lines[j]);
            if (answer !== null) {
                print(answer);
                return;
            }
        }
    }
    throw new Error("no candidates found");
}

run_part();
